// گرفتن همه منوها
let accordions = document.querySelectorAll('.accordion')
// برای همه منو ها در صفحه اعمال کن
accordions.forEach(accordion => {
    // تبدیل تگ های اچ تی ام ال به آرایه برای شناسایی تگها
    Array.from(accordion.children).forEach(wrapper => {
        // دسترسی به اندازه واقعی مکس هیت وقتی منو بازه
        if(wrapper.classList.contains('show')){
            wrapper.querySelector('div').style.maxHeight = wrapper.querySelector('div').scrollHeight + 30 + 'px'
        }
        // هروقت روی المنت عنوان منو کلیک شد
        wrapper.querySelector('span').addEventListener('click', e => {
            // دسترسی به عنوان منو
            let span = e.target
            // دسترسی به المنت والد منو
            let wrapper = span.parentElement
            // دسترسی به المنت محتوای منو
            let div = span.nextElementSibling
            // دادن کلاس باز و بسته شدن به المنت منو
            wrapper.classList.toggle('show')
            // اگر منو باز بود، مکث هیت واقعی بده بهش
            if (wrapper.classList.contains('show')){
                div.style.maxHeight = div.scrollHeight + 30 + 'px'
            } else {
                // وگرنه هیچی
                div.style.maxHeight = null
            }
            // بسته شدن منوی قبلی وقتی منوی دیگه باز میشه
            Array.from(accordion.children).forEach(w => {
                if (w != wrapper) {
                    w.classList.remove('show')
                    w.querySelector('div').style.maxHeight = null
                }
            })
        })
    })
})